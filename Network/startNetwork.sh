sh createCa.sh

sleep 5

echo "########## Setting required env ############"
export CHANNEL_NAME=tvchannel
export IMAGE_TAG=latest
export COMPOSE_PROJECT_NAME=fabricscratch-ca

echo "########## Generate the genesis block ############"
configtxgen -profile OrdererGenesis \
-channelID system-channel -outputBlock \
./channel-artifacts/genesis.block

sleep 2

echo "########## Generate the Channel Transaction ############"
configtxgen -profile TvChannel \
-outputCreateChannelTx ./channel-artifacts/$CHANNEL_NAME.tx \
-channelID $CHANNEL_NAME

sleep 2

echo "########## Starting the components ############"
docker-compose -f docker/docker-compose-singlepeer.yml up -d

export ORDERER_TLS_CA=`docker exec cli  env | grep ORDERER_TLS_CA | cut -d'=' -f2`

sleep 2

echo "########## Creating the Channel ############"
docker exec cli peer channel create -o orderer.tv.com:7050 \
-c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/peer/config/$CHANNEL_NAME.tx \
--tls --cafile $ORDERER_TLS_CA


sleep 2

echo "########## Joining Manufacturer Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ManufacturerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.manufacturer.tv.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining Dealer Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=DealerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.dealer.tv.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/users/Admin@dealer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining MVD Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=MvdMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.mvd.tv.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/users/Admin@mvd.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Generating anchor peer tx for manufacturer ############"
configtxgen -profile TvChannel -outputAnchorPeersUpdate ./channel-artifacts/ManufacturerMSPanchors.tx -channelID tvchannel -asOrg ManufacturerMSP

sleep 2

echo "########## Generating anchor peer tx for dealer ############"
configtxgen -profile TvChannel -outputAnchorPeersUpdate ./channel-artifacts/DealerMSPanchors.tx -channelID tvchannel -asOrg DealerMSP

sleep 2

echo "########## Generating anchor peer tx for dealer ############"
configtxgen -profile TvChannel -outputAnchorPeersUpdate ./channel-artifacts/MvdMSPanchors.tx -channelID tvchannel -asOrg MvdMSP

sleep 2

echo "########## Anchor Peer Update for Manufacturer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ManufacturerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.manufacturer.tv.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.tv.com:7050 -c tvchannel -f ./config/ManufacturerMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Dealer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=DealerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.dealer.tv.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/users/Admin@dealer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.tv.com:7050 -c tvchannel -f ./config/DealerMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Mvd ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=MvdMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.mvd.tv.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/users/Admin@mvd.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.tv.com:7050 -c tvchannel -f ./config/MvdMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Package Chaincode ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ManufacturerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.manufacturer.tv.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode package kbatv.tar.gz --path /opt/gopath/src/github.com/chaincode/tvContract/ --lang node --label kbatv_1

sleep 2

echo "##########  Install Chaincode on Manufacturer peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=ManufacturerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.manufacturer.tv.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbatv.tar.gz


sleep 2

echo "##########  Install Chaincode on Dealer peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=DealerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.dealer.tv.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/users/Admin@dealer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbatv.tar.gz

sleep 2

echo "##########  Install Chaincode on Mvd peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=MvdMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.mvd.tv.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/users/Admin@mvd.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbatv.tar.gz


echo "##########  Copy the above package ID for next steps, follow the approveCommit.txt ############"
