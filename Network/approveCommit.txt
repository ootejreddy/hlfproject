
export ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem

export CHANNEL_NAME=tvchannel

******************** Approve for Manufacturer Org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=ManufacturerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.manufacturer.tv.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID tvchannel \
     --collections-config /opt/gopath/src/github.com/chaincode/tvContract/collections.json \
     --name kbatv --version 1 --sequence 1 --signature-policy "OR('ManufacturerMSP.peer', 'DealerMSP.peer','MvdMSP.peer')" \
     --package-id kbatv_1:de997acad07a8a2684f0b824370c2927e6cb06f69e12fcbdf9715a9a0b6ee343 \
     --tls --cafile $ORDERER_TLS_CA \
     --waitForEvent

******************** Approve for Dealer Org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=DealerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.dealer.tv.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/users/Admin@dealer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID tvchannel \
     --name kbatv --version 1 --sequence 1 --signature-policy "OR('ManufacturerMSP.peer', 'DealerMSP.peer','MvdMSP.peer')"\
     --collections-config /opt/gopath/src/github.com/chaincode/tvContract/collections.json \
     --package-id kbatv_1:de997acad07a8a2684f0b824370c2927e6cb06f69e12fcbdf9715a9a0b6ee343  \
     --tls --cafile $ORDERER_TLS_CA --waitForEvent

******************** Approve for Mvd org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=MvdMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.mvd.tv.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/users/Admin@mvd.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID tvchannel \
     --name kbatv --version 1 --sequence 1 --signature-policy "OR('ManufacturerMSP.peer', 'DealerMSP.peer','MvdMSP.peer')"\
     --collections-config /opt/gopath/src/github.com/chaincode/tvContract/collections.json \
     --package-id kbatv_1:de997acad07a8a2684f0b824370c2927e6cb06f69e12fcbdf9715a9a0b6ee343  \
     --tls --cafile $ORDERER_TLS_CA --waitForEvent


******************** Check Commit Readiness ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=ManufacturerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.manufacturer.tv.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode checkcommitreadiness --channelID tvchannel \
     --name kbatv --version 1 --sequence 1 --tls --cafile $ORDERER_TLS_CA --output json

******************** Commit Chaincode ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=ManufacturerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.manufacturer.tv.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode commit -o orderer.tv.com:7050 -C tvchannel \
     --name kbatv --tls --cafile $ORDERER_TLS_CA \
     --peerAddresses peer0.manufacturer.tv.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     --peerAddresses peer0.dealer.tv.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/ca.crt \
     --peerAddresses peer0.mvd.tv.com:11051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/ca.crt \
     --collections-config /opt/gopath/src/github.com/chaincode/tvContract/collections.json \
     --version 1 --sequence 1 \
     --signature-policy "OR('ManufacturerMSP.peer', 'DealerMSP.peer','MvdMSP.peer')"





******************** Invoke Chaincode As Manufacturer Org Peer***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=ManufacturerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.manufacturer.tv.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer chaincode invoke -o orderer.tv.com:7050 --tls --cafile $ORDERER_TLS_CA \
     -C tvchannel -n kbatv \
     --peerAddresses peer0.manufacturer.tv.com:7051 \
     --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt \
     -c '{"function":"createTv", "Args":["TV001","LED","2020","LG"]}'

******************** Invoke Chaincode As Dealer Org Peer***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=DealerMSP \
     -e CHANNEL_NAME=tvchannel \
     -e CORE_PEER_ADDRESS=peer0.dealer.tv.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/users/Admin@dealer.tv.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer chaincode invoke -o orderer.tv.com:7050 --tls --cafile $ORDERER_TLS_CA \
     -C tvchannel -n kbatv \
     --peerAddresses peer0.dealer.tv.com:9051 \
     --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/ca.crt \
     -c '{"function":"createTv", "Args":["TV002","LCD","2021","Samsung"]}'
