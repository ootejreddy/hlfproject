/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const TvContract = require('./lib/tv-contract');
const OrderContract = require('./lib/order-contract');
module.exports.TvContract = TvContract;
module.exports.OrderContract = OrderContract;
module.exports.contracts = [ TvContract, OrderContract ];
