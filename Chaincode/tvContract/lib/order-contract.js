/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
// const crypto = require("crypto");

// async function getCollectionName(ctx) {
// const mspid = ctx.clientIdentity.getMSPID();
// const collectionName = `_implicit_org_${mspid}`;
// return 'collectionOrder';
// }

class OrderContract extends Contract {
    async orderExists(ctx, orderId) {
        const collectionName = 'CollectionOrder';
        const data = await ctx.stub.getPrivateDataHash(collectionName, orderId);
        return !!data && data.length > 0;
    }

    async createOrder(ctx, orderId) {
        const exists = await this.orderExists(ctx, orderId);
        if (exists) {
            throw new Error(`The asset order ${orderId} already exists`);
        }

        // const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (
            transientData.size === 0 ||
            !transientData.has('make') ||
            !transientData.has('modelNo')||
            !transientData.has('technology')||
            !transientData.has('dealerName')
        )
        {throw new Error(
            'The privateValue key was not specified in transient data. Please try again.'
        );}
        let orderAsset = {};
        orderAsset.make = transientData.get('make').toString();
        orderAsset.modelNo = transientData.get('modelNo').toString();
        orderAsset.technology = transientData.get('technology').toString();
        orderAsset.dealerName = transientData.get('dealerName').toString();
        orderAsset.type = 'order';

        orderAsset = {
            make:'LG',
            modelNo:'hY78',
            technology:'LCD',
            dealerName:'Deal-1',
            type:'order'
        };

        // privateAsset.privateValue = transientData
        //     .get('privateValue')
        //     .toString();

        // const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData(
            // eslint-disable-next-line no-undef
            'CollectionOrder',
            orderId,
            Buffer.from(JSON.stringify(orderAsset))
        );
    }

    async readOrder(ctx, orderId) {
        const exists = await this.orderExists(ctx, orderId);
        if (!exists) {
            throw new Error(`The asset order ${orderId} does not exist`);
        }

        // let privateDataString;
        // const collectionName = await getCollectionName(ctx);
        const orderData = await ctx.stub.getPrivateData(
            'CollectionOrder',
            orderId
        );
        return JSON.parse(orderData.toString());
        // return orderData;
    }

    // async updateOrder(ctx, orderId) {
    //     const exists = await this.orderExists(ctx, orderId);
    //     if (!exists) {
    //         throw new Error(`The asset order ${orderId} does not exist`);
    //     }
    //     const privateAsset = {};

    //     const transientData = ctx.stub.getTransient();
    //     if (transientData.size === 0 || !transientData.has('privateValue')) {
    //         throw new Error('The privateValue key was not specified in transient data. Please try again.');
    //     }
    //     privateAsset.privateValue = transientData.get('privateValue').toString();

    //     const collectionName = await getCollectionName(ctx);
    //     await ctx.stub.putPrivateData(collectionName, orderId, Buffer.from(JSON.stringify(privateAsset)));
    // }

    async deleteOrder(ctx, orderId) {
        const exists = await this.orderExists(ctx, orderId);
        if (!exists) {
            throw new Error(`The asset order ${orderId} does not exist`);
        }
        // const collectionName = await getCollectionName(ctx);
        await ctx.stub.deletePrivateData('CollectionOrder', orderId);
    }

    // async verifyOrder(ctx, mspid, orderId, objectToVerify) {

    //     // Convert provided object into a hash
    //     const hashToVerify = crypto.createHash('sha256').update(objectToVerify).digest('hex');
    //     const pdHashBytes = await ctx.stub.getPrivateDataHash(`_implicit_org_${mspid}`, orderId);
    //     if (pdHashBytes.length === 0) {
    //         throw new Error('No private data hash with the key: ' + orderId);
    //     }

    //     const actualHash = Buffer.from(pdHashBytes).toString('hex');

    //     // Compare the hash calculated (from object provided) and the hash stored on public ledger
    //     if (hashToVerify === actualHash) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}

module.exports = OrderContract;
