/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub,  } = require('fabric-shim');
const { TvContract } = require('..');
// const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
    }

}

describe('TvContract', () => {

    let contract;
    let ctx;

    //     beforeEach(() => {
    contract = new TvContract();
    ctx = new TestContext();
    ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"tv 1001 value"}'));
    // ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"tv 1002 value"}'));
    // });

    describe('#tvExists', () => {

        it('should return true for a tv', async () => {
            await contract.tvExists(ctx, '1001').should.eventually.be.true;

        });

    //     it('should return false for a tv that does not exist', async () => {
    //         await contract.tvExists(ctx, '1003').should.eventually.be.false;
    });

    // });

    //     describe('#createTv', () => {

    //         it('should create a tv', async () => {
    //             await contract.createTv(ctx, '1003', 'tv 1003 value');
    //             ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"tv 1003 value"}'));
    //         });

    //         it('should throw an error for a tv that already exists', async () => {
    //             await contract.createTv(ctx, '1001', 'myvalue').should.be.rejectedWith(/The tv 1001 already exists/);
    //         });

    //     });

    //     describe('#readTv', () => {

    //         it('should return a tv', async () => {
    //             await contract.readTv(ctx, '1001').should.eventually.deep.equal({ value: 'tv 1001 value' });
    //         });

    //         it('should throw an error for a tv that does not exist', async () => {
    //             await contract.readTv(ctx, '1003').should.be.rejectedWith(/The tv 1003 does not exist/);
    //         });

    //     });

    //     describe('#updateTv', () => {

    //         it('should update a tv', async () => {
    //             await contract.updateTv(ctx, '1001', 'tv 1001 new value');
    //             ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"tv 1001 new value"}'));
    //         });

    //         it('should throw an error for a tv that does not exist', async () => {
    //             await contract.updateTv(ctx, '1003', 'tv 1003 new value').should.be.rejectedWith(/The tv 1003 does not exist/);
    //         });

    //     });

    //     describe('#deleteTv', () => {

    //         it('should delete a tv', async () => {
    //             await contract.deleteTv(ctx, '1001');
    //             ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
    //         });

    //         it('should throw an error for a tv that does not exist', async () => {
    //             await contract.deleteTv(ctx, '1003').should.be.rejectedWith(/The tv 1003 does not exist/);
    //         });

    //     });

// });
});